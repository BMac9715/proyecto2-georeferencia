﻿namespace Proyecto_2_Georeferencia_ByD_
{
    partial class Cambios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblInformacion = new System.Windows.Forms.Label();
            this.txtNewCalle = new System.Windows.Forms.TextBox();
            this.txtNewAvenida = new System.Windows.Forms.TextBox();
            this.btnContinuar = new System.Windows.Forms.Button();
            this.lblCalleV = new System.Windows.Forms.Label();
            this.lblAvenidaV = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblInformacion
            // 
            this.lblInformacion.AutoSize = true;
            this.lblInformacion.Font = new System.Drawing.Font("Microsoft MHei", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInformacion.Location = new System.Drawing.Point(12, 18);
            this.lblInformacion.Name = "lblInformacion";
            this.lblInformacion.Size = new System.Drawing.Size(161, 17);
            this.lblInformacion.TabIndex = 0;
            this.lblInformacion.Text = "Modifique las coordenadas:";
            // 
            // txtNewCalle
            // 
            this.txtNewCalle.Location = new System.Drawing.Point(195, 52);
            this.txtNewCalle.Name = "txtNewCalle";
            this.txtNewCalle.Size = new System.Drawing.Size(68, 20);
            this.txtNewCalle.TabIndex = 1;
            this.txtNewCalle.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNewCalle_KeyPress);
            // 
            // txtNewAvenida
            // 
            this.txtNewAvenida.Location = new System.Drawing.Point(195, 82);
            this.txtNewAvenida.Name = "txtNewAvenida";
            this.txtNewAvenida.Size = new System.Drawing.Size(68, 20);
            this.txtNewAvenida.TabIndex = 2;
            this.txtNewAvenida.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNewAvenida_KeyPress);
            // 
            // btnContinuar
            // 
            this.btnContinuar.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnContinuar.Image = global::Proyecto_2_Georeferencia_ByD_.Properties.Resources.Save_as_48;
            this.btnContinuar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnContinuar.Location = new System.Drawing.Point(120, 123);
            this.btnContinuar.Name = "btnContinuar";
            this.btnContinuar.Size = new System.Drawing.Size(83, 42);
            this.btnContinuar.TabIndex = 4;
            this.btnContinuar.Text = "Guardar";
            this.btnContinuar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnContinuar.UseVisualStyleBackColor = false;
            this.btnContinuar.Click += new System.EventHandler(this.btnContinuar_Click);
            // 
            // lblCalleV
            // 
            this.lblCalleV.AutoSize = true;
            this.lblCalleV.Location = new System.Drawing.Point(82, 55);
            this.lblCalleV.Name = "lblCalleV";
            this.lblCalleV.Size = new System.Drawing.Size(91, 13);
            this.lblCalleV.TabIndex = 5;
            this.lblCalleV.Text = "Coordenada Calle";
            // 
            // lblAvenidaV
            // 
            this.lblAvenidaV.AutoSize = true;
            this.lblAvenidaV.Location = new System.Drawing.Point(82, 85);
            this.lblAvenidaV.Name = "lblAvenidaV";
            this.lblAvenidaV.Size = new System.Drawing.Size(107, 13);
            this.lblAvenidaV.TabIndex = 6;
            this.lblAvenidaV.Text = "Coordenada Avenida";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Proyecto_2_Georeferencia_ByD_.Properties.Resources.Car_50;
            this.pictureBox1.Location = new System.Drawing.Point(15, 55);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(48, 43);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // Cambios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(337, 181);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblAvenidaV);
            this.Controls.Add(this.lblCalleV);
            this.Controls.Add(this.btnContinuar);
            this.Controls.Add(this.txtNewAvenida);
            this.Controls.Add(this.txtNewCalle);
            this.Controls.Add(this.lblInformacion);
            this.Name = "Cambios";
            this.Text = "Cambios";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblInformacion;
        private System.Windows.Forms.TextBox txtNewCalle;
        private System.Windows.Forms.TextBox txtNewAvenida;
        private System.Windows.Forms.Button btnContinuar;
        private System.Windows.Forms.Label lblCalleV;
        private System.Windows.Forms.Label lblAvenidaV;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}