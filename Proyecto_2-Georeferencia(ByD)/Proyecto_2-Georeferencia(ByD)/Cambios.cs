﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto_2_Georeferencia_ByD_
{
    public partial class Cambios : Form
    {
        clsLector Informacion = new clsLector();

        string TipoVehiculo = "";
        int ArrayPosicion = 0;
        int CalleAnterior = 0;
        int AvenidaAnterior = 0;
        static bool confirmar = false;

        public Cambios()
        {
            InitializeComponent();
          
        }

        private void txtNewCalle_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) || char.IsControl(e.KeyChar))
            {
                if (e.KeyChar == Convert.ToChar(Keys.Enter))
                {
                    txtNewAvenida.Focus();
                }
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtNewAvenida_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) || char.IsControl(e.KeyChar))
            {
                if (e.KeyChar == Convert.ToChar(Keys.Enter))
                {
                    btnContinuar.Focus();
                }
            }           
            else
            {
                e.Handled = true;
            }

        }

        private void btnContinuar_Click(object sender, EventArgs e)
        {
            
            if(Convert.ToInt32(txtNewCalle.Text) <= 0 || Convert.ToInt32(txtNewCalle.Text) > 31 || Convert.ToInt32(txtNewAvenida.Text) <= 0 || Convert.ToInt32(txtNewAvenida.Text) > 31)
            {
                MessageBox.Show("Error, valores fuera de los limites establecidos. Recuerde que los valores ingresados deben ser positivo, menores a 31.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtNewCalle.Focus();
                return;
            }

            if (Convert.ToInt32(txtNewCalle.Text) % 2 == 0 && Convert.ToInt32(txtNewAvenida.Text) % 2 == 0)
            {
                MessageBox.Show("Error, coordenadas dedicadas a edificios, ingrese otra coordenada","ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtNewCalle.Focus();
            }
            else
            {
                if (VerificarPosicion(Convert.ToInt32(txtNewCalle.Text), Convert.ToInt32(txtNewAvenida.Text)))
                {
                    MessageBox.Show("Error, estas coordenadas estan ocupadas por otro vehiculo.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtNewCalle.Focus();
                }
                else
                {                   
                    switch (TipoVehiculo)
                    {
                        case "V":
                            Informacion.LeerRegistroVehiculos()[ArrayPosicion].Calle = Convert.ToInt32(txtNewCalle.Text);
                            Informacion.LeerRegistroVehiculos()[ArrayPosicion].Avenida = Convert.ToInt32(txtNewAvenida.Text);
                            confirmar = true;
                            break;
                        case "P":
                            Informacion.LeerRegistroPolicias()[ArrayPosicion].Calle = Convert.ToInt32(txtNewCalle.Text);
                            Informacion.LeerRegistroPolicias()[ArrayPosicion].Avenida = Convert.ToInt32(txtNewAvenida.Text);
                            confirmar = true;
                            break;
                        case "B":
                            Informacion.LeerRegistroBombero()[ArrayPosicion].Calle = Convert.ToInt32(txtNewCalle.Text);
                            Informacion.LeerRegistroBombero()[ArrayPosicion].Avenida = Convert.ToInt32(txtNewAvenida.Text);
                            confirmar = true;
                            break;
                    }
                }

                this.Close();
            }

        }

        public void Identificar(string tipo, int posicion)
        {
            TipoVehiculo = tipo;
            ArrayPosicion = posicion;

            switch (TipoVehiculo)
            {
                case "V":
                    txtNewCalle.Text = Convert.ToString(Informacion.LeerRegistroVehiculos()[ArrayPosicion].Calle);
                    txtNewAvenida.Text = Convert.ToString(Informacion.LeerRegistroVehiculos()[ArrayPosicion].Avenida);
                    CalleAnterior = Convert.ToInt32(txtNewCalle.Text);
                    AvenidaAnterior = Convert.ToInt32(txtNewAvenida.Text);
                    txtNewCalle.Focus();
                    break;
                case "P":
                    txtNewCalle.Text = Convert.ToString(Informacion.LeerRegistroPolicias()[ArrayPosicion].Calle);
                    txtNewAvenida.Text = Convert.ToString(Informacion.LeerRegistroPolicias()[ArrayPosicion].Avenida);
                    CalleAnterior = Convert.ToInt32(txtNewCalle.Text);
                    AvenidaAnterior = Convert.ToInt32(txtNewAvenida.Text);
                    txtNewCalle.Focus();
                    break;
                case "B":
                    txtNewCalle.Text = Convert.ToString(Informacion.LeerRegistroBombero()[ArrayPosicion].Calle);
                    txtNewAvenida.Text = Convert.ToString(Informacion.LeerRegistroBombero()[ArrayPosicion].Avenida);
                    CalleAnterior = Convert.ToInt32(txtNewCalle.Text);
                    AvenidaAnterior = Convert.ToInt32(txtNewAvenida.Text);
                    txtNewCalle.Focus();
                    break;
            }
        }

        private bool VerificarPosicion(int calle, int avenida)
        {
            for(int i = 0; i < 70; i++)
            {
                if(Informacion.LeerRegistroVehiculos()[i].Calle == calle && Informacion.LeerRegistroVehiculos()[i].Avenida == avenida)
                {
                    return true;
                }
            }

            for(int x = 0; x < 20; x++)
            {
                if (Informacion.LeerRegistroPolicias()[x].Calle == calle && Informacion.LeerRegistroPolicias()[x].Avenida == avenida)
                {
                    return true;
                }

                if(Informacion.LeerRegistroBombero()[x].Calle == calle && Informacion.LeerRegistroBombero()[x].Avenida == avenida)
                {
                    return true;
                }

            }

            return false;

        }

        public bool ConfimarMovimiento()
        {
            return confirmar;
        }

    }
}
