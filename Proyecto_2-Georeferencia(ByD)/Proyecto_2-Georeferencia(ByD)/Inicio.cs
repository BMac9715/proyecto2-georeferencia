﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto_2_Georeferencia_ByD_
{
    public partial class Proyecto_Georeferencia : Form
    {
        clsLector CargarArchivo = new clsLector();

        public Proyecto_Georeferencia()
        {
            InitializeComponent();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnArchivo_Click(object sender, EventArgs e)
        {           
            string ruta = "";
            
            OpenFileDialog OpenFile = new OpenFileDialog(); //Abre el explorador de archivos

            if (OpenFile.ShowDialog() == DialogResult.OK)
            {
                ruta = OpenFile.FileName;
            }

            if (CargarArchivo.Ejecutar(ruta))
            {
                MessageBox.Show(CargarArchivo.errores(), "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                MessageBox.Show(" Por favor, verifica tu archivo y vuelve a cargarlo.", "INFORMACION",MessageBoxButtons.OK,MessageBoxIcon.Information);
                btnArchivo.Focus();
            }
            else
            {
                MessageBox.Show(" Lectura de datos finalizada exitosamente. \n Se han cargado los elementos al programa.", "LECTURA DE ARCHIVO COMPLETA", MessageBoxButtons.OK, MessageBoxIcon.Information);
                btnMapa.Enabled = true;
                btnMapa.Focus();
            }
            
        }

        private void btnMapa_Click(object sender, EventArgs e)
        {
            Mapa Iniciar = new Mapa();
            this.Hide();
            Iniciar.Show();

            btnMapa.Enabled = false;
            btnArchivo.Enabled = false;
            btnSalir.Focus();

        }

    }
}
