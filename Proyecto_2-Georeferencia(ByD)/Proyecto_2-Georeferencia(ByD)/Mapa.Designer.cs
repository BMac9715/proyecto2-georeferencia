﻿namespace Proyecto_2_Georeferencia_ByD_
{
    partial class Mapa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gBAcciones = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.gBCiudad = new System.Windows.Forms.GroupBox();
            this.lblHoras = new System.Windows.Forms.Label();
            this.lblFecha = new System.Windows.Forms.Label();
            this.dgvCiudad = new System.Windows.Forms.DataGridView();
            this.TimerTrafico = new System.Windows.Forms.Timer(this.components);
            this.gBAcciones.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gBCiudad.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCiudad)).BeginInit();
            this.SuspendLayout();
            // 
            // gBAcciones
            // 
            this.gBAcciones.Controls.Add(this.groupBox1);
            this.gBAcciones.Location = new System.Drawing.Point(12, 22);
            this.gBAcciones.Name = "gBAcciones";
            this.gBAcciones.Size = new System.Drawing.Size(265, 645);
            this.gBAcciones.TabIndex = 0;
            this.gBAcciones.TabStop = false;
            this.gBAcciones.Text = "Opciones";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Location = new System.Drawing.Point(17, 30);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(227, 152);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(49, 32);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(89, 42);
            this.button1.TabIndex = 0;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // gBCiudad
            // 
            this.gBCiudad.Controls.Add(this.lblHoras);
            this.gBCiudad.Controls.Add(this.lblFecha);
            this.gBCiudad.Controls.Add(this.dgvCiudad);
            this.gBCiudad.Location = new System.Drawing.Point(300, 22);
            this.gBCiudad.Name = "gBCiudad";
            this.gBCiudad.Size = new System.Drawing.Size(1031, 645);
            this.gBCiudad.TabIndex = 1;
            this.gBCiudad.TabStop = false;
            this.gBCiudad.Text = "Mapa";
            // 
            // lblHoras
            // 
            this.lblHoras.AutoSize = true;
            this.lblHoras.Location = new System.Drawing.Point(903, 626);
            this.lblHoras.Name = "lblHoras";
            this.lblHoras.Size = new System.Drawing.Size(35, 13);
            this.lblHoras.TabIndex = 2;
            this.lblHoras.Text = "Horas";
            // 
            // lblFecha
            // 
            this.lblFecha.AutoSize = true;
            this.lblFecha.Location = new System.Drawing.Point(959, 626);
            this.lblFecha.Name = "lblFecha";
            this.lblFecha.Size = new System.Drawing.Size(37, 13);
            this.lblFecha.TabIndex = 1;
            this.lblFecha.Text = "Fecha";
            // 
            // dgvCiudad
            // 
            this.dgvCiudad.AllowUserToAddRows = false;
            this.dgvCiudad.AllowUserToOrderColumns = true;
            this.dgvCiudad.AllowUserToResizeColumns = false;
            this.dgvCiudad.AllowUserToResizeRows = false;
            this.dgvCiudad.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvCiudad.ColumnHeadersVisible = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvCiudad.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvCiudad.GridColor = System.Drawing.SystemColors.HotTrack;
            this.dgvCiudad.Location = new System.Drawing.Point(15, 19);
            this.dgvCiudad.Name = "dgvCiudad";
            this.dgvCiudad.ReadOnly = true;
            this.dgvCiudad.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dgvCiudad.RowHeadersVisible = false;
            this.dgvCiudad.Size = new System.Drawing.Size(1000, 604);
            this.dgvCiudad.TabIndex = 0;
            this.dgvCiudad.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCiudad_CellClick);
            // 
            // TimerTrafico
            // 
            this.TimerTrafico.Enabled = true;
            this.TimerTrafico.Tick += new System.EventHandler(this.TimerTrafico_Tick);
            // 
            // Mapa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1354, 690);
            this.Controls.Add(this.gBCiudad);
            this.Controls.Add(this.gBAcciones);
            this.Name = "Mapa";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mapa";
            this.gBAcciones.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.gBCiudad.ResumeLayout(false);
            this.gBCiudad.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCiudad)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gBAcciones;
        private System.Windows.Forms.GroupBox gBCiudad;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lblHoras;
        private System.Windows.Forms.Label lblFecha;
        private System.Windows.Forms.Timer TimerTrafico;
        private System.Windows.Forms.DataGridView dgvCiudad;
    }
}