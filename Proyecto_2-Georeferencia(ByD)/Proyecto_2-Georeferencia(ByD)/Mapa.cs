﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace Proyecto_2_Georeferencia_ByD_
{
    public partial class Mapa : Form
    {
        int AvenidasCiudad = 31;
        int CallesCiudad = 31;
        

        clsCityManager City = new clsCityManager();
        clsLector Coordenadas = new clsLector();      

        public Mapa()
        {
            // Retarda el inicio del form Mapa para que se pueda simular el Form Pantalla Carga
            Thread Tardar = new Thread(new ThreadStart(PantallaCarga)); 
            Tardar.Start();
            Thread.Sleep(12000);

            //Inicia el form
            InitializeComponent();
            this.Show();

            //Cierra el form Pantalla Carga
            Tardar.Abort();
            CargarMapa();
            DrawCiudad();
            TimerTrafico.Enabled = true;        
        }

        // Llama la simulacion de pantalla de carga
        public void PantallaCarga()
        {
            Application.Run(new PantallaCarga());
        }

        public void CargarMapa()
        {
            //Carga vehiculos al mapa
            for (int i = 0; i < 70; i++)
            {
               City.PintarVehiculos(Coordenadas.LeerRegistroVehiculos()[i].Calle, Coordenadas.LeerRegistroVehiculos()[i].Avenida);
            }

            //Carga Hospitales al mapa
            for(int w = 0; w < 20; w++)
            {
                City.PintarHospitales(Coordenadas.LeerRegistroHospitales()[w].Calle, Coordenadas.LeerRegistroHospitales()[w].Avenida);
            }

            //Carga Restaurantes al mapa
            for(int x = 0; x < 50; x++)
            {
                City.PintarRestaurantes(Coordenadas.LeerRegistroRestaurante()[x].Calle, Coordenadas.LeerRegistroRestaurante()[x].Avenida);
            }

            //Carga Gasolineras al mapa
            for(int b = 0; b < 50; b++)
            {
                City.PintarGasolineras(Coordenadas.LeerRegistroGasolinera()[b].Calle, Coordenadas.LeerRegistroGasolinera()[b].Avenida);
            }

            //Carga Policias al mapa
            for(int a = 0; a < 20; a++)
            {
                City.PintarPoicias(Coordenadas.LeerRegistroPolicias()[a].Calle, Coordenadas.LeerRegistroPolicias()[a].Avenida);
            }

            //Carga Bomberos al mapa
            for(int f = 0; f < 20; f++)
            {
                City.PintarBomberos(Coordenadas.LeerRegistroBombero()[f].Calle, Coordenadas.LeerRegistroBombero()[f].Avenida);
            }

        }

        public void DrawCiudad()
        {
            dgvCiudad.Columns.Clear();
            dgvCiudad.Rows.Clear();

            //Ciclo que agrega las columnas del datagridview
            for (int a = 0; a < AvenidasCiudad; a++)
            {
                dgvCiudad.Columns.Add("Columna " + Convert.ToString(a), "");
                dgvCiudad.Columns[a].Width = 75;
            }

            for (int i = 0; i < AvenidasCiudad; i++) 
            {
                dgvCiudad.Rows.Add();
                dgvCiudad.Rows[i].Height = 75;

                for (int j = 0; j < CallesCiudad; j++)
                {
                    switch (City.ObtenerCiudad()[i, j])
                    {
                        case "Ab":
                            dgvCiudad.Rows[i].Cells[j].Style.BackColor = Color.DarkGray;
                            break;
                        case "Arr":
                            dgvCiudad.Rows[i].Cells[j].Style.BackColor = Color.DarkGray;
                            break;

                        case "Iz":
                            dgvCiudad.Rows[i].Cells[j].Style.BackColor = Color.DarkGray;
                            break;
                        case "De":
                            dgvCiudad.Rows[i].Cells[j].Style.BackColor = Color.DarkGray;
                            break;

                        case "M":
                            DataGridViewImageCell Municipalidad = new DataGridViewImageCell();
                            Municipalidad.ImageLayout = DataGridViewImageCellLayout.Stretch;
                            dgvCiudad[15, 15] = Municipalidad;
                            dgvCiudad[15, 15].Style.BackColor = Color.LightGreen;
                            Bitmap Muni = new Bitmap(Properties.Resources.Municipalidad);
                            dgvCiudad[15, 15].Value = Muni;
                            break;

                        case "V":
                            DataGridViewImageCell Vehiculos = new DataGridViewImageCell();
                            Vehiculos.ImageLayout = DataGridViewImageCellLayout.Normal;
                            dgvCiudad[j, i] = Vehiculos;
                            dgvCiudad[j, i].Style.BackColor = Color.DarkGray;
                            Random elegir = new Random();
                            int Num = elegir.Next(1,6);

                            if(Num == 6)
                            {
                                MessageBox.Show("Hola puto, te pise :v");
                            }

                            switch(Num)
                            {
                                case 1:
                                    Bitmap DibujoCarro = new Bitmap(Properties.Resources.Auto5);
                                    dgvCiudad[j, i].Value = DibujoCarro;
                                    break;
                                case 2:
                                    Bitmap DibujoCarro1= new Bitmap(Properties.Resources.Auto2);
                                    dgvCiudad[j, i].Value = DibujoCarro1;
                                    break;
                                case 3:
                                    Bitmap DibujoCarro2 = new Bitmap(Properties.Resources.Auto3);
                                    dgvCiudad[j, i].Value = DibujoCarro2;
                                    break;
                                case 4:
                                    Bitmap DibujoCarro3 = new Bitmap(Properties.Resources.Auto4);
                                    dgvCiudad[j, i].Value = DibujoCarro3;
                                    break;
                                case 5:
                                    Bitmap DibujoCarro4 = new Bitmap(Properties.Resources.Auto1);
                                    dgvCiudad[j, i].Value = DibujoCarro4;
                                    break;
                            }
                           
                            break;

                        case "G":
                            DataGridViewImageCell Gasolinera = new DataGridViewImageCell();
                            Gasolinera.ImageLayout = DataGridViewImageCellLayout.Stretch;
                            dgvCiudad[j, i] = Gasolinera;
                            Bitmap DibujoGasolinera = new Bitmap(Properties.Resources.Gasolinera);
                            dgvCiudad[j, i].Value = DibujoGasolinera;
                            break;
                        case "H":
                            DataGridViewImageCell Hospital = new DataGridViewImageCell();
                            Hospital.ImageLayout = DataGridViewImageCellLayout.Stretch;
                            dgvCiudad[j, i] = Hospital;
                            Bitmap DibujoHospital = new Bitmap(Properties.Resources.Hospital_3_48);
                            dgvCiudad[j, i].Value = DibujoHospital;
                            break;
                        case "R":
                            DataGridViewImageCell Restaurante = new DataGridViewImageCell();
                            Restaurante.ImageLayout = DataGridViewImageCellLayout.Stretch;
                            dgvCiudad[j, i] = Restaurante;
                            dgvCiudad[j, i].Style.BackColor = Color.DarkGray;
                            Bitmap DibujoRestaurante = new Bitmap(Properties.Resources.Restaurant_Membership_Card_48);
                            dgvCiudad[j, i].Value = DibujoRestaurante;
                            break;
                        case "P":
                            DataGridViewImageCell Policias = new DataGridViewImageCell();
                            Policias.ImageLayout = DataGridViewImageCellLayout.Normal;
                            dgvCiudad[j, i] = Policias;
                            dgvCiudad[j, i].Style.BackColor = Color.DarkGray;
                            Bitmap DibujoPolicias = new Bitmap(Properties.Resources.Policia);
                            dgvCiudad[j, i].Value = DibujoPolicias;
                            break;
                        case "B":
                            DataGridViewImageCell Ambulancias = new DataGridViewImageCell();
                            Ambulancias.ImageLayout = DataGridViewImageCellLayout.Normal;
                            dgvCiudad[j, i] = Ambulancias;
                            dgvCiudad[j, i].Style.BackColor = Color.DarkGray;
                            Bitmap DibujoAmbulancia= new Bitmap(Properties.Resources.Ambulancia);
                            dgvCiudad[j, i].Value = DibujoAmbulancia;
                            break;
                        case " ":
                            DataGridViewImageCell Terrenos = new DataGridViewImageCell();
                            Terrenos.ImageLayout = DataGridViewImageCellLayout.Stretch;
                            dgvCiudad[j, i] = Terrenos;
                            Bitmap DibujoVacio = new Bitmap(Properties.Resources.tierras_baldías);
                            dgvCiudad[j, i].Value = DibujoVacio;
                            break;
                    }
                }
            }         
        }

        private void dgvCiudad_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int column = dgvCiudad.CurrentCell.ColumnIndex;
            int row = dgvCiudad.CurrentCell.RowIndex;

            switch (City.MostrarInformacion(column, row))
            {
                case "V":

                    #region Informacion Vehiculos
                    //Compara la casilla seleccionada, basandose en su valor.
                    //Cuando encuentra la posicion en el arreglo de Vehiculos, extrae los datos y los muestra en pantalla atraves de un MessageBox
                    for (int i = 0; i < 70; i++)
                    {
                        if(Coordenadas.LeerRegistroVehiculos()[i].Calle == (column+1) && Coordenadas.LeerRegistroVehiculos()[i].Avenida == (row + 1))
                        {
                           DialogResult Resultado;

                           Resultado = MessageBox.Show(" Placa: " + Coordenadas.LeerRegistroVehiculos()[i].Placa +
                                            "\n Marca: " + Coordenadas.LeerRegistroVehiculos()[i].Marca +
                                            "\n Velocidad: " + Convert.ToString(Coordenadas.LeerRegistroVehiculos()[i].Velocidad) + " km/h" +
                                            "\n Ubicacion: " + Convert.ToString(Coordenadas.LeerRegistroVehiculos()[i].Calle) + " Calle y " +
                                            Convert.ToString(Coordenadas.LeerRegistroVehiculos()[i].Avenida) + " Avenida." +
                                            "\n \n ¿Deseas modificar la ubicación del vehiculo?"
                                            , " INFORMACION DEL VEHICULO",MessageBoxButtons.YesNo,MessageBoxIcon.Information);
                            
                            if(Resultado == DialogResult.Yes)
                            {
                                Cambios Mostrar = new Cambios();
                                Mostrar.Identificar("V", i);
                                Mostrar.ShowDialog();
                                if (Mostrar.ConfimarMovimiento())
                                {
                                    CargarMapa();
                                    DrawCiudad();
                                }

                            }
                            else
                            {
                                return;
                            }
                        }
                    }
                    #endregion
                    break;

                case "R":

                    #region Informacion Restaurantes
                    //Compara la casilla seleccionada, basandose en su valor.
                    //Cuando encuentra la posicion en el arreglo de restaurantes, extrae los datos y los muestra en pantalla atraves de un MessageBox
                    string tipo = "";

                    for(int x = 0; x < 50; x++)
                    {
                        if (Coordenadas.LeerRegistroRestaurante()[x].Calle == (column + 1) && Coordenadas.LeerRegistroRestaurante()[x].Avenida == (row + 1))
                        {
                            //Compara su valor de tipo int para saber que tipo de comida sirven en el lugar
                            switch(Coordenadas.LeerRegistroRestaurante()[x].Tipo)
                            {
                                case 1:
                                    tipo = "Comida Rápida";
                                    break;
                                case 2:
                                    tipo = "Pizzerias";
                                    break;
                                case 3:
                                    tipo = "Comida Gourmet";
                                    break;
                            }

                          

                            //Muestra el mensaje en pantalla
                            MessageBox.Show(" Nombre: " + Coordenadas.LeerRegistroRestaurante()[x].Nombre +
                                            "\n Tipo de Comida: " + tipo +
                                            "\n Ubicacion: " + Convert.ToString(Coordenadas.LeerRegistroRestaurante()[x].Calle) + " Calle y " +
                                            Convert.ToString(Coordenadas.LeerRegistroRestaurante()[x].Avenida) + " Avenida.", " INFORMACION DE RESTAURANTES", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        }
                    }
                    #endregion

                    break;

                case "H":

                    #region Informacion Hospitales
                    //Compara la casilla seleccionada, basandose en su valor.
                    //Cuando encuentra la posicion en el arreglo de Hospitales, extrae los datos y los muestra en pantalla atraves de un MessageBox
                    string Clase = "";

                    for (int b = 0; b < 20; b++)
                    {
                      
                        if (Coordenadas.LeerRegistroHospitales()[b].Calle == (column + 1) && Coordenadas.LeerRegistroHospitales()[b].Avenida == (row + 1))
                        {
                            //Debe saber, si es privado o publico, por ello compara por medio de un if el valor de tipo int que tiene ingresado.
                            if (Coordenadas.LeerRegistroHospitales()[b].private_or_public == 1)
                            {
                                Clase = "Privado";
                            }
                            else
                            {
                                Clase = "Público";
                            }

                            //Muestra el mensaje
                            MessageBox.Show(" Nombre: " + Coordenadas.LeerRegistroHospitales()[b].Nombre +
                                            "\n Tipo: " + Clase +
                                            "\n Ubicacion: " + Convert.ToString(Coordenadas.LeerRegistroHospitales()[b].Calle) + " Calle y " +
                                            Convert.ToString(Coordenadas.LeerRegistroHospitales()[b].Avenida) + " Avenida.", " INFORMACION DE HOSPITALES", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    #endregion

                    break;
                case "G":

                    #region Informacion Gasolineras
                    //Compara la casilla seleccionada, basandose en su valor.
                    //Cuando encuentra la posicion en el arreglo de gasolineras, extrae los datos y los muestra en pantalla atraves de un MessageBox
                    for (int j = 0; j < 50; j++)
                    {
                        if (Coordenadas.LeerRegistroGasolinera()[j].Calle == (column + 1) && Coordenadas.LeerRegistroGasolinera()[j].Avenida == (row + 1))
                        {
                            MessageBox.Show(" Nombre: " + Coordenadas.LeerRegistroGasolinera()[j].Nombre +
                                            "\n Precio de la gasolina: Q." + Convert.ToString(Coordenadas.LeerRegistroGasolinera()[j].Precio) + 
                                            "\n Ubicacion: " + Convert.ToString(Coordenadas.LeerRegistroGasolinera()[j].Calle) + " Calle y " +
                                            Convert.ToString(Coordenadas.LeerRegistroGasolinera()[j].Avenida) + " Avenida.", " INFORMACION DE GASOLINERAS", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    #endregion

                    break;
                case "P":

                    #region Informacion Policias

                    //Compara la casilla seleccionada, basandose en su valor.
                    //Cuando encuentra la posicion en el arreglo de policias, extrae los datos y los muestra en pantalla atraves de un MessageBox
                    for (int k = 0; k < 20; k++)
                    {
                        if (Coordenadas.LeerRegistroPolicias()[k].Calle == (column + 1) && Coordenadas.LeerRegistroPolicias()[k].Avenida == (row + 1))
                        {
                            DialogResult Resultado;
                            Resultado = MessageBox.Show(" Nombre: " + Coordenadas.LeerRegistroPolicias()[k].Nombre +                   
                                            "\n Ubicacion: " + Convert.ToString(Coordenadas.LeerRegistroPolicias()[k].Calle) + " Calle y " +
                                            Convert.ToString(Coordenadas.LeerRegistroPolicias()[k].Avenida) + " Avenida." +
                                            "\n \n ¿Deseas modificar la ubicación de la patrulla?", " INFORMACION DE POLICIAS", MessageBoxButtons.YesNo, MessageBoxIcon.Information);                            
                          
                            if (Resultado == DialogResult.Yes)
                            {
                                Cambios Mostrar = new Cambios();
                                Mostrar.Identificar("P", k);
                                Mostrar.ShowDialog();
                                if(Mostrar.ConfimarMovimiento())
                                {
                                    CargarMapa();
                                    DrawCiudad();
                                }                               
                            }
                            else
                            {
                                return;
                            }
                        }
                    }

                    #endregion

                    break;
                  
                case "B":

                    #region Informacion Bomberos
                    //Compara la casilla seleccionada, basandose en su valor.
                    //Cuando encuentra la posicion en el arreglo de bomberos, extrae los datos y los muestra en pantalla atraves de un MessageBox
                    for (int a = 0; a < 20; a++)
                    {
                        if (Coordenadas.LeerRegistroBombero()[a].Calle == (column + 1) && Coordenadas.LeerRegistroBombero()[a].Avenida == (row + 1))
                        {
                            DialogResult Resultado;

                            Resultado = MessageBox.Show(" Nombre: " + Coordenadas.LeerRegistroBombero()[a].Nombre +
                                            "\n Ubicacion: " + Convert.ToString(Coordenadas.LeerRegistroBombero()[a].Calle) + " Calle y " +
                                            Convert.ToString(Coordenadas.LeerRegistroBombero()[a].Avenida) + " Avenida." +
                                            "\n \n ¿Deseas modificar la ubicación de la ambulancia?", " INFORMACION DE POLICIAS", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                            if (Resultado == DialogResult.Yes)
                            {
                                Cambios Mostrar = new Cambios();
                                Mostrar.Identificar("B", a);
                                Mostrar.ShowDialog();
                                if (Mostrar.ConfimarMovimiento())
                                {
                                    CargarMapa();
                                    DrawCiudad();
                                }
                            }
                            else
                            {
                                return;
                            }
                        }
                    }
                    #endregion

                    break;
            }
        }

        private void TimerTrafico_Tick(object sender, EventArgs e)
        {
            lblFecha.Text = DateTime.Now.ToString("dd/MM/yy");
            lblHoras.Text = DateTime.Now.ToString("hh:mm tt");
        }
    }
}
