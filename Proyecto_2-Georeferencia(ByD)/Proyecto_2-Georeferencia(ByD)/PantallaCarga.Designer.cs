﻿namespace Proyecto_2_Georeferencia_ByD_
{
    partial class PantallaCarga
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pgBarPantallaCarga = new System.Windows.Forms.ProgressBar();
            this.TimerCarga = new System.Windows.Forms.Timer(this.components);
            this.lblTexto = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // pgBarPantallaCarga
            // 
            this.pgBarPantallaCarga.Location = new System.Drawing.Point(181, 241);
            this.pgBarPantallaCarga.Name = "pgBarPantallaCarga";
            this.pgBarPantallaCarga.Size = new System.Drawing.Size(527, 30);
            this.pgBarPantallaCarga.TabIndex = 0;
            // 
            // TimerCarga
            // 
            this.TimerCarga.Enabled = true;
            this.TimerCarga.Tick += new System.EventHandler(this.TimerCarga_Tick);
            // 
            // lblTexto
            // 
            this.lblTexto.AutoSize = true;
            this.lblTexto.Font = new System.Drawing.Font("Yu Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTexto.Location = new System.Drawing.Point(258, 312);
            this.lblTexto.Name = "lblTexto";
            this.lblTexto.Size = new System.Drawing.Size(359, 23);
            this.lblTexto.TabIndex = 1;
            this.lblTexto.Text = "Construyendo la ciudad..... Por favor espere....";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Proyecto_2_Georeferencia_ByD_.Properties.Resources.Imagen_Fondo_Construccion;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(885, 382);
            this.Controls.Add(this.lblTexto);
            this.Controls.Add(this.pgBarPantallaCarga);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form2";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form2";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar pgBarPantallaCarga;
        private System.Windows.Forms.Timer TimerCarga;
        private System.Windows.Forms.Label lblTexto;
    }
}