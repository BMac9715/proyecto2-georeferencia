﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto_2_Georeferencia_ByD_
{
    public partial class PantallaCarga : Form
    {
        public PantallaCarga()
        {
            InitializeComponent();
        }

        private void TimerCarga_Tick(object sender, EventArgs e)
        {
            this.pgBarPantallaCarga.Increment(1);
            if(pgBarPantallaCarga.Value == 1000)
            {
                this.TimerCarga.Stop();
            }
        }
    }
}
