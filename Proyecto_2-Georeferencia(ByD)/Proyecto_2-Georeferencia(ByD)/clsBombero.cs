﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto_2_Georeferencia_ByD_
{
    class clsBombero
    {
        string _Nombre = "";
        int _Calle = 0;
        int _Avenida = 0;

        public clsBombero()
        {

        }

        public string Nombre
        {
            get
            {
                return _Nombre;
            }
            set
            {
                _Nombre = value;
            }
        }

        public int Calle
        {
            get
            {
                return _Calle;
            }
            set
            {
                _Calle = value;
            }
        }

        public int Avenida
        {
            get
            {
                return _Avenida;
            }
            set
            {
                _Avenida = value;
            }
        }
    }
}
