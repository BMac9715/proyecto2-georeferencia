﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto_2_Georeferencia_ByD_
{
    class clsGasolinera
    {
        string _Nombre = "";
        double _Precio = 0;
        int _Calle = 0;
        int _Avenida = 0;

        public clsGasolinera()
        {
        }

        public string Nombre
        {
            get
            {
                return _Nombre;
            }
            set
            {
                _Nombre = value;
            }
        }

        public double Precio
        {
            get
            {
                return _Precio;
            }
            set
            {
                _Precio = value;
            }
        }

        public int Calle
        {
            get
            {
                return _Calle;
            }
            set
            {
                _Calle = value;
            }
        }

        public int Avenida
        {
            get
            {
                return _Avenida;
            }
            set
            {
                _Avenida = value;
            }
        }

    }
}
