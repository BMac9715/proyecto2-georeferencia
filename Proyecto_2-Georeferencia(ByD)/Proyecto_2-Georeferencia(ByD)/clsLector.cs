﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Proyecto_2_Georeferencia_ByD_
{
    class clsLector
    {
        int c = 0;
        string ErrorAux = "";

        // Instancia de los objetos a utilizar
        clsVehiculo Vehiculo;
        clsRestaurante Restaurante;
        clsHospital Hospital;
        clsGasolinera Gasolinera;
        clsPolicia OficialPolicia;
        clsBombero Bombero;

        //Arreglos de los objetos
        //Se encargan de guardar los datos recibidos del .txt
        static clsVehiculo[] RVehiculos = new clsVehiculo[70];
        static clsRestaurante[] RRestaurantes = new clsRestaurante[50];
        static clsHospital[] RHospitales = new clsHospital[20];
        static clsGasolinera[] RGasolinera = new clsGasolinera[50];
        static clsPolicia[] RPolicias = new clsPolicia[20];
        static clsBombero[] RBomberos = new clsBombero[20];

        //Arreglo de contadores...
        int[] Counts = new int[6];
        bool[] Errores = new bool[6];  //Arreglo de los errores que puedan existir durante la carga de datos...

        // Contienen los datos de los diferentes
        
        public bool Ejecutar(string Ubicacion1)
        {
            Leer(Ubicacion1);
            ComprobacionPosiciones();

            for (int i = 0; i < Errores.Length; i++)
            {
                if(Errores[i] != false)
                {
                    return true;
                }
            }

            return false;
        }

        private void Leer(string Ubicacion)
        {
            string UbicaciónArchivo = "";
            string Lineas = "";
          
            try
            {
                UbicaciónArchivo = Ubicacion;
                StreamReader Archivo = new StreamReader(UbicaciónArchivo);

                while (Archivo.Peek() != -1)
                {
                    Lineas = Archivo.ReadLine();

                    switch (Lineas)
                    {
                        case "":

                            break;
                        case "[VEHICULOS]":

                            #region Vehiculos

                            Lineas = Archivo.ReadLine();
                            Counts[0] = 0;  // Contador que se encarga de contabilizar cuantos autos se van a ingresar

                            while (Lineas != "[FIN]")   //Termina  la lectura cuando encuentre la linea que tenga el texto [FIN]
                            {
                                if (Counts[0] > 70)
                                {
                                    Errores[0] = true;
                                }
                                else
                                {
                                    if (Lineas == "")
                                    {
                                        Lineas = Archivo.ReadLine();
                                    }
                                    else
                                    {
                                        char[] Limitador = { '/' };
                                        string[] Palabras = Lineas.Split(Limitador);

                                        Vehiculo = new clsVehiculo();
                                        Vehiculo.Placa = Palabras[0];
                                        Vehiculo.Marca = Palabras[1];
                                        Vehiculo.Velocidad = Convert.ToInt32(Palabras[2]);
                                        Vehiculo.Calle = Convert.ToInt32(Palabras[3]);
                                        Vehiculo.Avenida = Convert.ToInt32(Palabras[4]);

                                        RVehiculos[Counts[0]] = Vehiculo;

                                        Lineas = Archivo.ReadLine();
                                        Counts[0]++;
                                    }
                                }
                            }
                            #endregion

                            break;
                        case "[RESTAURANTES]":

                            #region Restaurantes

                            Lineas = Archivo.ReadLine();
                            Counts[1] = 0;  // Contador que se encarga de contabilizar cuantos restaurantes se van a ingresar

                            while (Lineas != "[FIN]")   //Termina  la lectura cuando encuentre la linea que tenga el texto [FIN]
                            {
                                if (Counts[1] > 50)
                                {
                                    Errores[0] = true;
                                }
                                else
                                {
                                    if (Lineas == "")
                                    {
                                        Lineas = Archivo.ReadLine();
                                    }
                                    else
                                    {
                                        char[] Limitador = { '/' };
                                        string[] Palabras = Lineas.Split(Limitador);

                                        Restaurante = new clsRestaurante();
                                        Restaurante.Nombre = Palabras[0];
                                        Restaurante.Tipo = Convert.ToInt32(Palabras[1]);
                                        Restaurante.Calle = Convert.ToInt32(Palabras[2]);
                                        Restaurante.Avenida = Convert.ToInt32(Palabras[3]);

                                        RRestaurantes[Counts[1]] = Restaurante;

                                        Lineas = Archivo.ReadLine();
                                        Counts[1]++;
                                    }
                                }
                            }
                            #endregion

                            break;
                        case "[HOSPITALES]":

                            #region Hospitales

                            Lineas = Archivo.ReadLine();
                            Counts[2] = 0;  // Contador que se encarga de contabilizar cuantos hospitales se van a ingresar

                            while (Lineas != "[FIN]")   //Termina  la lectura cuando encuentre la linea que tenga el texto [FIN]
                            {
                                if (Counts[2] > 20)
                                {
                                    Errores[0] = true;
                                }
                                else
                                {
                                    if (Lineas == "")
                                    {
                                        Lineas = Archivo.ReadLine();
                                    }
                                    else
                                    {
                                        char[] Limitador = { '/' };
                                        string[] Palabras = Lineas.Split(Limitador);

                                        Hospital = new clsHospital();
                                        Hospital.Nombre = Palabras[0];
                                        Hospital.private_or_public = Convert.ToInt32(Palabras[1]);
                                        Hospital.Calle = Convert.ToInt32(Palabras[2]);
                                        Hospital.Avenida = Convert.ToInt32(Palabras[3]);

                                        RHospitales[Counts[2]] = Hospital;

                                        Lineas = Archivo.ReadLine();
                                        Counts[2]++;
                                    }
                                }
                            }

                            #endregion

                            break;

                        case "[GASOLINERAS]":

                            #region Gasolineras

                            Lineas = Archivo.ReadLine();
                            Counts[3] = 0;  // Contador que se encarga de contabilizar cuantos gasolineras se van a ingresar

                            while (Lineas != "[FIN]")   //Termina  la lectura cuando encuentre la linea que tenga el texto [FIN]
                            {
                                if (Counts[3] > 50)
                                {
                                    Errores[0] = true;
                                }
                                else
                                {
                                    if (Lineas == "")
                                    {
                                        Lineas = Archivo.ReadLine();
                                    }
                                    else
                                    {
                                        char[] Limitador = { '/' };
                                        string[] Palabras = Lineas.Split(Limitador);

                                        Gasolinera = new clsGasolinera();
                                        Gasolinera.Nombre = Palabras[0];
                                        Gasolinera.Precio = Convert.ToDouble(Palabras[1]);
                                        Gasolinera.Calle = Convert.ToInt32(Palabras[2]);
                                        Gasolinera.Avenida = Convert.ToInt32(Palabras[3]);

                                        RGasolinera[Counts[3]] = Gasolinera;

                                        Lineas = Archivo.ReadLine();
                                        Counts[3]++;
                                    }
                                }
                            }

                            #endregion

                            break;
                        case "[POLICIA]":

                            #region Oficiales de policia

                            Lineas = Archivo.ReadLine();
                            Counts[4] = 0;  // Contador que se encarga de contabilizar cuantos gasolineras se van a ingresar

                            while (Lineas != "[FIN]")   //Termina  la lectura cuando encuentre la linea que tenga el texto [FIN]
                            {
                                if (Counts[4] > 19)
                                {
                                    Errores[0] = true;
                                    Lineas = Archivo.ReadLine();
                                }
                                else
                                {
                                    if (Lineas == "")
                                    {
                                        Lineas = Archivo.ReadLine();
                                    }
                                    else
                                    {
                                        char[] Limitador = { '/' };
                                        string[] Palabras = Lineas.Split(Limitador);

                                        OficialPolicia = new clsPolicia();

                                        OficialPolicia.Nombre = Palabras[0];
                                        OficialPolicia.Calle = Convert.ToInt32(Palabras[1]);
                                        OficialPolicia.Avenida = Convert.ToInt32(Palabras[2]);

                                        RPolicias[Counts[4]] = OficialPolicia;

                                        Lineas = Archivo.ReadLine();
                                        Counts[4]++;
                                    }
                                }
                            }
                            #endregion

                            break;
                        case "[BOMBERO]":

                            #region Bomberos

                            Lineas = Archivo.ReadLine();
                            Counts[5] = 0;  // Contador que se encarga de contabilizar cuantos gasolineras se van a ingresar

                            while (Lineas != "[FIN]")   //Termina  la lectura cuando encuentre la linea que tenga el texto [FIN]
                            {
                                if (Counts[5] > 20)
                                {
                                    Errores[0] = true;
                                }
                                else
                                {
                                    if (Lineas == "")
                                    {
                                        Lineas = Archivo.ReadLine();
                                    }
                                    else
                                    {
                                        char[] Limitador = { '/' };
                                        string[] Palabras = Lineas.Split(Limitador);

                                        Bombero = new clsBombero();

                                        Bombero.Nombre = Palabras[0];
                                        Bombero.Calle = Convert.ToInt32(Palabras[1]);
                                        Bombero.Avenida = Convert.ToInt32(Palabras[2]);

                                        RBomberos[Counts[5]] = Bombero;

                                        Lineas = Archivo.ReadLine();
                                        Counts[5]++;
                                    }
                                }
                            }

                            #endregion

                            break;
                    }
                }

                Archivo.Close();
               
            }
            catch
            {
                Errores[1] = true;
                ErrorAux = Lineas;
            }
        }

        private void ComprobacionPosiciones()
        {
            // Comprueba que no se repita las mismas coordenadas para los vehiculos, bomberos y policias

            #region Comprobar Vehiculos, Policias y Bomberos

           // Recorre el arreglo de los objetos vehiculos y verifica que no sean iguales

            for (int x = 0; x < Counts[0]; x++)
            {
                if((RVehiculos[x].Calle%2) == 0 && (RVehiculos[x].Avenida%2) == 0)
                {
                    Errores[4] = true;
                }

                if (RVehiculos[x].Calle > 31 || RVehiculos[x].Avenida > 31 || RVehiculos[x].Calle <= 0 || RVehiculos[x].Avenida <= 0)
                {
                    Errores[3] = true;
                }

                for (int y = Counts[0] - 1; y >= 0; y--)
                {
                    if(RVehiculos[x].Placa == RVehiculos[y].Placa)
                    {
                        if(x != y)
                        {
                            Errores[5] = true;
                        }                     
                    }

                    if (RVehiculos[x].Calle == RVehiculos[y].Calle && RVehiculos[x].Avenida == RVehiculos[y].Avenida)
                    {
                        if (x != y)
                        {
                            Errores[2] = true;
                        }
                    }
                }

                // Policias
                for (int d = 0; d < Counts[4]; d++)
                {
                    if (RVehiculos[x].Calle == RPolicias[d].Calle && RVehiculos[x].Avenida == RPolicias[d].Avenida)
                    {
                        Errores[2] = true;
                    }
                }
                // Bomberos
                for (int e = 0; e < Counts[5]; e++)
                {
                    if (RVehiculos[x].Calle == RBomberos[e].Calle && RVehiculos[x].Avenida == RBomberos[e].Avenida)
                    {
                        Errores[2] = true;
                    }
                }
            }

            #endregion

            #region Comprobar Policias

            for (int f = 0; f < Counts[4]; f++)
            {
                if ((RPolicias[f].Calle % 2) == 0 && (RPolicias[f].Avenida % 2) == 0)
                {
                    Errores[4] = true;
                }

                if (RPolicias[f].Calle > 31 || RPolicias[f].Avenida > 31 || RPolicias[f].Calle <= 0 || RPolicias[f].Avenida <= 0)
                {
                    Errores[3] = true;
                }

                for(int g = Counts[4]-1; g >= 0; g--)
                {
                    if(RPolicias[f].Calle == RPolicias[g].Calle && RPolicias[f].Avenida == RPolicias[g].Avenida)
                    {
                        if(f != g)
                        {
                            Errores[2] = true;
                        }
                    }
                }

                // Policias y bomberos
                for (int h = 0; h < Counts[5]; h++)
                {
                    if(RPolicias[f].Calle == RBomberos[h].Calle && RPolicias[f].Avenida == RBomberos[h].Avenida)
                    {
                        Errores[2] = true;
                    }
                }
            }

            #endregion

            #region Comprobar Bomberos

            for (int f = 0; f < Counts[5]; f++)
            {
                if ((RBomberos[f].Calle % 2) == 0 && (RBomberos[f].Avenida % 2) == 0)
                {
                    Errores[4] = true;
                }

                if (RBomberos[f].Calle > 31 || RBomberos[f].Avenida > 31 || RBomberos[f].Calle <= 0 || RBomberos[f].Avenida <= 0)
                {
                    Errores[3] = true;
                }
                for (int g = Counts[5] - 1; g >= 0; g--)
                {
                    if (RBomberos[f].Calle == RBomberos[g].Calle && RBomberos[f].Avenida == RBomberos[g].Avenida)
                    {
                        if (f != g)
                        {
                            Errores[2] = true;
                        }
                    }
                }
            }

            #endregion

            // Comprobacion de la ubicacion de los terrrenos 

            #region Comprobar Hospitales

            for (int f = 0; f < Counts[2]; f++)
            {
                if (RHospitales[f].Calle > 31 || RHospitales[f].Calle <= 0 || RHospitales[f].Avenida > 31 || RHospitales[f].Avenida <= 0)
                {
                    Errores[3] = true;
                }
                //Revisa su propio arreglo para verificar que no se repitan coordenadas
                for (int g = Counts[2] - 1; g >= 0; g--)
                {
                    if (RHospitales[f].Calle == RHospitales[g].Calle && RHospitales[f].Avenida == RHospitales[g].Avenida)
                    {
                        if (f != g)
                        {
                            Errores[2] = true;
                        }
                    }
                }

                // Revisa los datos de restaurantes y los compara para verificar que no existan coordenadas iguales...
                for (int i = 0; i < Counts[1]; i++)
                {
                    if (RHospitales[f].Calle == RRestaurantes[i].Calle && RHospitales[f].Avenida == RRestaurantes[i].Avenida)
                    {
                        Errores[2] = true;
                    }
                }

                // Revisa los datos de Gasolineras y los compara para verificar que no existan coordenadas iguales...

                for (int j = 0; j < Counts[3]; j++)
                {
                    if (RHospitales[f].Calle == RGasolinera[j].Calle && RHospitales[f].Avenida == RGasolinera[j].Avenida)
                    {
                        Errores[2] = true;
                    }
                }
            }

                #endregion

            #region Comprobar Restaurantes

            for (int f = 0; f < Counts[1]; f++)
            {
                if (RRestaurantes[f].Calle > 31 || RRestaurantes[f].Calle <= 0 || RRestaurantes[f].Avenida > 31 || RRestaurantes[f].Avenida <= 0)
                {
                    Errores[3] = true;
                }

                //Revisa su propio arreglo para verificar que no se repitan coordenadas
                for (int g = Counts[1] - 1; g >= 0; g--)
                {
                    if (RRestaurantes[f].Calle == RRestaurantes[g].Calle && RRestaurantes[f].Avenida == RRestaurantes[g].Avenida)
                    {
                        if (f != g)
                        {
                            Errores[2] = true;
                        }
                    }
                }

                // Revisa los datos de Gasolineras y los compara para verificar que no existan coordenadas iguales...
                for (int k = 0; k < Counts[3]; k++)
                {
                    if (RRestaurantes[f].Calle == RGasolinera[k].Calle && RRestaurantes[f].Avenida == RGasolinera[k].Avenida)
                    {
                       Errores[2] = true;
                    }
                }
            }

            #endregion

            #region Comprobar Gasolineras

                for (int f = 0; f < Counts[3]; f++)
                {
                    if (RGasolinera[f].Calle > 31 || RGasolinera[f].Calle <= 0 || RGasolinera[f].Avenida > 31 || RGasolinera[f].Avenida <= 0)
                    {
                        Errores[3] = true;
                    }
                    for (int g = Counts[3] - 1; g >= 0; g--)
                    {
                        if (RGasolinera[f].Calle == RGasolinera[g].Calle && RGasolinera[f].Avenida == RGasolinera[g].Avenida)
                        {
                            if (f != g)
                            {
                                Errores[2] = true;
                            }
                        }
                    }
                }

                #endregion

        }

        public string errores()
        {
            string ErroresGeneral = "";
            string ErrorCero = "";
            string ErrorUno = "";
            string ErrorDos = "";
            string ErrorTres = "";
            string ErrorCuatro = "";
            string ErrorCinco = "";

            #region Error0

            if(Errores[0])
            {
                ErrorCero = " AVISO 0001. Se han sobrepasado los limites de elementos al cargar el archivo. \n Los elementos son:";
                
                if(Counts[0] > RVehiculos.Length)
                {
                    ErrorCero += "\n [VEHICULOS] ";
                }

                if(Counts[1]> RRestaurantes.Length)
                {
                    ErrorCero += "\t [RESTAURANTES]";
                }

                if(Counts[2] > RHospitales.Length)
                {
                    ErrorCero += "\t [HOSPITALES]";
                }

                if(Counts[3] > RGasolinera.Length)
                {
                    ErrorCero += "\t [GASOLINERAS]";
                }

                if(Counts[4] > RPolicias.Length -1)
                {
                    ErrorCero += "\t [POLICIA]";
                }

                if(Counts[5] > RBomberos.Length)
                {
                    ErrorCero += "\t [BOMBERO]";
                }

                ErrorCero += " \n \n Por favor verifique que la cantidad de datos sea correcto. \n Se han cargado solamente los elementos que se encontraban en el limite.";
            }

            #endregion

            #region Error1

            if (Errores[1])
            {
                ErrorUno = "ERROR 0001. Ha ocurrido un error al leer el archivo. \n El formato  de la linea " + "'" + ErrorAux + "' no es correcto. Verifiquelo.";
            }

            #endregion

            #region Error2        

            if (Errores[2])
            {
                ErrorDos = "ERROR 0002. Ha ocurrido un error, las coordenadas de ubicación de los elementos, no pueden ser las mismas.";
            }

            #endregion

            #region Error3

            if(Errores[3])
            {
                ErrorTres = "ERROR 0003. Se han superado los limites en las coordenadas de los elementos. \nPor favor revise sus datos y recuerde que las coordenadas deben estar entre el rango de 1 a 31.";
            }

            #endregion

            #region Error4

            if(Errores[4])
            {
                ErrorCuatro = "ERROR 0004. No pueden existir edificios en las calles y de la misma forma no \nno pueden existir vehiculos, patrullas o ambulancias en terrenos dedicados a edificos ";
            }

            #endregion

            #region Error5
            if (Errores[5])
            {
                ErrorCinco = "ERROR 0005. VEHICULOS, los números de placa no pueden ser iguales.";
            }
            #endregion

            if (ErrorDos == "" && ErrorCero == "" && ErrorTres == "" && ErrorCuatro == "" && ErrorCinco == "")
            {
                return ErrorUno;
            }

            if (ErrorUno == "" && ErrorCero == "" && ErrorDos == "" && ErrorTres == "" && ErrorCinco == "")
            {
                return ErrorCuatro;
            }

            if (ErrorUno == "" && ErrorCero == "" && ErrorDos == "" && ErrorCuatro == "" && ErrorCinco == "")
            {
                return ErrorTres;
            }

            if (ErrorCero == "" && ErrorUno == "" && ErrorTres == "" && ErrorCuatro == "" && ErrorCinco == "")
            {
                return ErrorDos;
            }

            if (ErrorCero == "" && ErrorUno == "" && ErrorTres == "" && ErrorCuatro == "" && ErrorDos == "")
            {
                return ErrorCinco;
            }

            if (ErrorCero != "" && ErrorUno != "")
            {
                ErroresGeneral = ErrorCero + "\n \n" + ErrorUno;
                return ErroresGeneral;
            }

            if(ErrorUno != "" && ErrorDos != "")
            {
                ErroresGeneral = ErrorUno + "\n \n" + ErrorDos;
                return ErroresGeneral;
            }

            if(ErrorDos != "" && ErrorTres != "")
            {
                ErroresGeneral = ErrorDos + "\n \n" + ErrorTres;
                return ErroresGeneral;
            }
            if (ErrorCero != "" && ErrorCuatro != "")
            {
                ErroresGeneral = ErrorCero + "\n \n" + ErrorCuatro;
                return ErroresGeneral;
            }

            ErroresGeneral = ErrorCero + "\n \n" + ErrorUno + "\n \n" + ErrorDos + "\n \n" + ErrorTres +"\n \n" + ErrorCuatro + "\n \n" + ErrorCinco;

            return ErroresGeneral;
        }

        public clsVehiculo[] LeerRegistroVehiculos()
        {
            return RVehiculos;
        }

        public clsRestaurante[] LeerRegistroRestaurante()
        {
            return RRestaurantes;
        }

        public clsHospital[] LeerRegistroHospitales()
        {
            return RHospitales;
        }

        public clsGasolinera[] LeerRegistroGasolinera()
        {
            return RGasolinera;
        }

        public clsPolicia[] LeerRegistroPolicias()
        {
            return RPolicias;
        }

        public clsBombero[] LeerRegistroBombero()
        {
            return RBomberos;
        }

    }
}
