﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto_2_Georeferencia_ByD_
{
    class clsVehiculo
    {
        private string _Placa;
        private string _Marca;
        private int _Velocidad;
        private int _Calle;
        private int _Avenida;
        private string _Color;

        public clsVehiculo ()
        {
        }

        public string Placa
        {
            get
            {
                return _Placa;
            }
            set
            {
                _Placa = value;
            }
        }

        public string Marca
        {
            get
            {
                return _Marca;
            }
            set
            {
                _Marca = value;
            }
        }

        public int Velocidad
        {
            get
            {
                return _Velocidad;
            }
            set
            {
                _Velocidad = value;
            }
        }

        public int Calle
        {
            get
            {
                return _Calle;
            }
            set
            {
                _Calle = value;
            }
        }

        public int Avenida
        {
            get
            {
                return _Avenida;
            }
            set
            {
                _Avenida = value;
            }
        }

        public string Color
        {
            get
            {
                return _Color;
            }
            set
            {
                _Color = value;
            }
        }

    }
}
